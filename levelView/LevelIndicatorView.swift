//
//  LevelIndicatorView.swift
//  levelView
//
//  Created by Stanisław Paśkowski on 23.03.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

@IBDesignable class LevelIndicatorView: UIView {
    
    let color: UIColor = UIColor(red:0.67, green:0.71, blue:0.74, alpha:1.0)
    var difficultyLevel: Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, difficultyLevel: Int) {
        self.init(frame: frame)
        self.difficultyLevel = difficultyLevel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        let rectWidth: CGFloat = rect.width - 2
        let spacingWidth: CGFloat = (rectWidth + rectWidth / 5 / 4) / 5
        let elementWidth: CGFloat = (rectWidth - spacingWidth) / 5
        
        var elementHeight: CGFloat = elementWidth
        
        var spacing: CGFloat = 0
        for index in 0...4 {
            if index == 0 {
                spacing = spacing + 1
            }
        
            let path = UIBezierPath(rect: CGRect(x:spacing, y: -1 + rect.height - elementHeight, width: elementWidth, height: elementHeight))
            
            color.setStroke()
            color.setFill()
            path.lineWidth = 1
            path.stroke()
            if index < difficultyLevel {
                path.fill()
            }
            spacing += spacingWidth
            elementHeight += elementWidth
        }
    }
}
